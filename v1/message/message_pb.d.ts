// package: fcp.message.v1
// file: v1/message/message.proto

import * as jspb from "google-protobuf";
import * as v1_message_common_dtos_pb from "../../v1/message_common/dtos_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class Message extends jspb.Message {
  getFrom(): string;
  setFrom(value: string): void;

  getTo(): string;
  setTo(value: string): void;

  getMessageType(): MessageTypeMap[keyof MessageTypeMap];
  setMessageType(value: MessageTypeMap[keyof MessageTypeMap]): void;

  hasSendAt(): boolean;
  clearSendAt(): void;
  getSendAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setSendAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasMessageChat(): boolean;
  clearMessageChat(): void;
  getMessageChat(): v1_message_common_dtos_pb.ChatMessage | undefined;
  setMessageChat(value?: v1_message_common_dtos_pb.ChatMessage): void;

  hasGroupChat(): boolean;
  clearGroupChat(): void;
  getGroupChat(): v1_message_common_dtos_pb.GroupsChange | undefined;
  setGroupChat(value?: v1_message_common_dtos_pb.GroupsChange): void;

  hasUserStatus(): boolean;
  clearUserStatus(): void;
  getUserStatus(): v1_message_common_dtos_pb.UserStatus | undefined;
  setUserStatus(value?: v1_message_common_dtos_pb.UserStatus): void;

  getMessageBodyCase(): Message.MessageBodyCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Message.AsObject;
  static toObject(includeInstance: boolean, msg: Message): Message.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Message, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Message;
  static deserializeBinaryFromReader(message: Message, reader: jspb.BinaryReader): Message;
}

export namespace Message {
  export type AsObject = {
    from: string,
    to: string,
    messageType: MessageTypeMap[keyof MessageTypeMap],
    sendAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    messageChat?: v1_message_common_dtos_pb.ChatMessage.AsObject,
    groupChat?: v1_message_common_dtos_pb.GroupsChange.AsObject,
    userStatus?: v1_message_common_dtos_pb.UserStatus.AsObject,
  }

  export enum MessageBodyCase {
    MESSAGE_BODY_NOT_SET = 0,
    MESSAGE_CHAT = 11,
    GROUP_CHAT = 12,
    USER_STATUS = 13,
  }
}

export class MessageItem extends jspb.Message {
  getMessageType(): MessageTypeMap[keyof MessageTypeMap];
  setMessageType(value: MessageTypeMap[keyof MessageTypeMap]): void;

  getMessageBody(): string;
  setMessageBody(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageItem.AsObject;
  static toObject(includeInstance: boolean, msg: MessageItem): MessageItem.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MessageItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageItem;
  static deserializeBinaryFromReader(message: MessageItem, reader: jspb.BinaryReader): MessageItem;
}

export namespace MessageItem {
  export type AsObject = {
    messageType: MessageTypeMap[keyof MessageTypeMap],
    messageBody: string,
  }
}

export class StreamMessages extends jspb.Message {
  clearMessageList(): void;
  getMessageList(): Array<MessageItem>;
  setMessageList(value: Array<MessageItem>): void;
  addMessage(value?: MessageItem, index?: number): MessageItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamMessages.AsObject;
  static toObject(includeInstance: boolean, msg: StreamMessages): StreamMessages.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StreamMessages, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamMessages;
  static deserializeBinaryFromReader(message: StreamMessages, reader: jspb.BinaryReader): StreamMessages;
}

export namespace StreamMessages {
  export type AsObject = {
    messageList: Array<MessageItem.AsObject>,
  }
}

export interface MessageTypeMap {
  MESSAGETYPE_NONE: 0;
  MESSAGETYPE_CHATSEND: 1;
  MESSAGETYPE_CHATEDIT: 2;
  MESSAGETYPE_CHATDELETE: 3;
  MESSAGETYPE_CHATGROUPCREATE: 4;
  MESSAGETYPE_CHATGROUPDELETE: 5;
  MESSAGETYPE_CHATGROUPUPDATE: 6;
  MESSAGETYPE_CHATADDUSERGROUP: 7;
  MESSAGETYPE_CHATDELETEUSERGROUP: 8;
  MESSAGETYPE_USERCHANGESTATUS: 9;
}

export const MessageType: MessageTypeMap;

