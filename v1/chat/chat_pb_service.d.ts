// package: fcp.message.v1
// file: v1/chat/chat.proto

import * as v1_chat_chat_pb from "../../v1/chat/chat_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ChatServiceGetGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.GetGroupRequest;
  readonly responseType: typeof v1_chat_chat_pb.GetGroupResponse;
};

type ChatServiceCreateGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.CreateGroupRequest;
  readonly responseType: typeof v1_chat_chat_pb.CreateGroupResponse;
};

type ChatServiceUpdateGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.UpdateGroupRequest;
  readonly responseType: typeof v1_chat_chat_pb.UpdateGroupResponse;
};

type ChatServiceListGroups = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.ListGroupsRequest;
  readonly responseType: typeof v1_chat_chat_pb.ListGroupsResponse;
};

type ChatServiceDeleteGroups = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.DeleteGroupsRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type ChatServiceMutingGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.MutingGroupRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type ChatServiceAddUserGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.AddUserGroupRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type ChatServiceDeleteUserGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.DeleteUserGroupRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type ChatServiceGetUserGroups = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.GetUserGroupsRequest;
  readonly responseType: typeof v1_chat_chat_pb.GetUserGroupsResponse;
};

type ChatServiceGetHistory = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.GetHistoryRequest;
  readonly responseType: typeof v1_chat_chat_pb.GetHistoryResponse;
};

type ChatServiceSendMessage = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.SendMessageRequest;
  readonly responseType: typeof v1_chat_chat_pb.SendMessageResponse;
};

type ChatServiceUpdateMessage = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.UpdateMessageRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type ChatServiceDeleteMessage = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.DeleteMessageRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type ChatServiceSearchGroups = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.SearchGroupsRequest;
  readonly responseType: typeof v1_chat_chat_pb.SearchGroupsResponse;
};

type ChatServiceSearchMessagesInGroup = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_chat_chat_pb.SearchMessagesInGroupRequest;
  readonly responseType: typeof v1_chat_chat_pb.SearchMessagesInGroupResponse;
};

export class ChatService {
  static readonly serviceName: string;
  static readonly GetGroup: ChatServiceGetGroup;
  static readonly CreateGroup: ChatServiceCreateGroup;
  static readonly UpdateGroup: ChatServiceUpdateGroup;
  static readonly ListGroups: ChatServiceListGroups;
  static readonly DeleteGroups: ChatServiceDeleteGroups;
  static readonly MutingGroup: ChatServiceMutingGroup;
  static readonly AddUserGroup: ChatServiceAddUserGroup;
  static readonly DeleteUserGroup: ChatServiceDeleteUserGroup;
  static readonly GetUserGroups: ChatServiceGetUserGroups;
  static readonly GetHistory: ChatServiceGetHistory;
  static readonly SendMessage: ChatServiceSendMessage;
  static readonly UpdateMessage: ChatServiceUpdateMessage;
  static readonly DeleteMessage: ChatServiceDeleteMessage;
  static readonly SearchGroups: ChatServiceSearchGroups;
  static readonly SearchMessagesInGroup: ChatServiceSearchMessagesInGroup;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ChatServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getGroup(
    requestMessage: v1_chat_chat_pb.GetGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.GetGroupResponse|null) => void
  ): UnaryResponse;
  getGroup(
    requestMessage: v1_chat_chat_pb.GetGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.GetGroupResponse|null) => void
  ): UnaryResponse;
  createGroup(
    requestMessage: v1_chat_chat_pb.CreateGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.CreateGroupResponse|null) => void
  ): UnaryResponse;
  createGroup(
    requestMessage: v1_chat_chat_pb.CreateGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.CreateGroupResponse|null) => void
  ): UnaryResponse;
  updateGroup(
    requestMessage: v1_chat_chat_pb.UpdateGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.UpdateGroupResponse|null) => void
  ): UnaryResponse;
  updateGroup(
    requestMessage: v1_chat_chat_pb.UpdateGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.UpdateGroupResponse|null) => void
  ): UnaryResponse;
  listGroups(
    requestMessage: v1_chat_chat_pb.ListGroupsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.ListGroupsResponse|null) => void
  ): UnaryResponse;
  listGroups(
    requestMessage: v1_chat_chat_pb.ListGroupsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.ListGroupsResponse|null) => void
  ): UnaryResponse;
  deleteGroups(
    requestMessage: v1_chat_chat_pb.DeleteGroupsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteGroups(
    requestMessage: v1_chat_chat_pb.DeleteGroupsRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  mutingGroup(
    requestMessage: v1_chat_chat_pb.MutingGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  mutingGroup(
    requestMessage: v1_chat_chat_pb.MutingGroupRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  addUserGroup(
    requestMessage: v1_chat_chat_pb.AddUserGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  addUserGroup(
    requestMessage: v1_chat_chat_pb.AddUserGroupRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteUserGroup(
    requestMessage: v1_chat_chat_pb.DeleteUserGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteUserGroup(
    requestMessage: v1_chat_chat_pb.DeleteUserGroupRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  getUserGroups(
    requestMessage: v1_chat_chat_pb.GetUserGroupsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.GetUserGroupsResponse|null) => void
  ): UnaryResponse;
  getUserGroups(
    requestMessage: v1_chat_chat_pb.GetUserGroupsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.GetUserGroupsResponse|null) => void
  ): UnaryResponse;
  getHistory(
    requestMessage: v1_chat_chat_pb.GetHistoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.GetHistoryResponse|null) => void
  ): UnaryResponse;
  getHistory(
    requestMessage: v1_chat_chat_pb.GetHistoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.GetHistoryResponse|null) => void
  ): UnaryResponse;
  sendMessage(
    requestMessage: v1_chat_chat_pb.SendMessageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.SendMessageResponse|null) => void
  ): UnaryResponse;
  sendMessage(
    requestMessage: v1_chat_chat_pb.SendMessageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.SendMessageResponse|null) => void
  ): UnaryResponse;
  updateMessage(
    requestMessage: v1_chat_chat_pb.UpdateMessageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  updateMessage(
    requestMessage: v1_chat_chat_pb.UpdateMessageRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteMessage(
    requestMessage: v1_chat_chat_pb.DeleteMessageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteMessage(
    requestMessage: v1_chat_chat_pb.DeleteMessageRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  searchGroups(
    requestMessage: v1_chat_chat_pb.SearchGroupsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.SearchGroupsResponse|null) => void
  ): UnaryResponse;
  searchGroups(
    requestMessage: v1_chat_chat_pb.SearchGroupsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.SearchGroupsResponse|null) => void
  ): UnaryResponse;
  searchMessagesInGroup(
    requestMessage: v1_chat_chat_pb.SearchMessagesInGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.SearchMessagesInGroupResponse|null) => void
  ): UnaryResponse;
  searchMessagesInGroup(
    requestMessage: v1_chat_chat_pb.SearchMessagesInGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_chat_chat_pb.SearchMessagesInGroupResponse|null) => void
  ): UnaryResponse;
}

