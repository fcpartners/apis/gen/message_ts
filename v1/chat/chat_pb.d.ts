// package: fcp.message.v1
// file: v1/chat/chat.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_message_common_dtos_pb from "../../v1/message_common/dtos_pb";
import * as v1_message_common_common_pb from "../../v1/message_common/common_pb";

export class SearchGroupsRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationRequest): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_message_common_common_pb.Sort | undefined;
  setSort(value?: v1_message_common_common_pb.Sort): void;

  getPattern(): string;
  setPattern(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchGroupsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SearchGroupsRequest): SearchGroupsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchGroupsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchGroupsRequest;
  static deserializeBinaryFromReader(message: SearchGroupsRequest, reader: jspb.BinaryReader): SearchGroupsRequest;
}

export namespace SearchGroupsRequest {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationRequest.AsObject,
    sort?: v1_message_common_common_pb.Sort.AsObject,
    pattern: string,
  }
}

export class SearchGroupsResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationResponse): void;

  clearGroupsList(): void;
  getGroupsList(): Array<v1_message_common_dtos_pb.GroupBrief>;
  setGroupsList(value: Array<v1_message_common_dtos_pb.GroupBrief>): void;
  addGroups(value?: v1_message_common_dtos_pb.GroupBrief, index?: number): v1_message_common_dtos_pb.GroupBrief;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchGroupsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SearchGroupsResponse): SearchGroupsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchGroupsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchGroupsResponse;
  static deserializeBinaryFromReader(message: SearchGroupsResponse, reader: jspb.BinaryReader): SearchGroupsResponse;
}

export namespace SearchGroupsResponse {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationResponse.AsObject,
    groupsList: Array<v1_message_common_dtos_pb.GroupBrief.AsObject>,
  }
}

export class SearchMessagesInGroupRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationRequest): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_message_common_common_pb.Sort | undefined;
  setSort(value?: v1_message_common_common_pb.Sort): void;

  getGroupId(): number;
  setGroupId(value: number): void;

  getPattern(): string;
  setPattern(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchMessagesInGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SearchMessagesInGroupRequest): SearchMessagesInGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchMessagesInGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchMessagesInGroupRequest;
  static deserializeBinaryFromReader(message: SearchMessagesInGroupRequest, reader: jspb.BinaryReader): SearchMessagesInGroupRequest;
}

export namespace SearchMessagesInGroupRequest {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationRequest.AsObject,
    sort?: v1_message_common_common_pb.Sort.AsObject,
    groupId: number,
    pattern: string,
  }
}

export class SearchMessagesInGroupResponse extends jspb.Message {
  clearMessagesList(): void;
  getMessagesList(): Array<v1_message_common_dtos_pb.ChatMessage>;
  setMessagesList(value: Array<v1_message_common_dtos_pb.ChatMessage>): void;
  addMessages(value?: v1_message_common_dtos_pb.ChatMessage, index?: number): v1_message_common_dtos_pb.ChatMessage;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchMessagesInGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SearchMessagesInGroupResponse): SearchMessagesInGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchMessagesInGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchMessagesInGroupResponse;
  static deserializeBinaryFromReader(message: SearchMessagesInGroupResponse, reader: jspb.BinaryReader): SearchMessagesInGroupResponse;
}

export namespace SearchMessagesInGroupResponse {
  export type AsObject = {
    messagesList: Array<v1_message_common_dtos_pb.ChatMessage.AsObject>,
  }
}

export class GetGroupRequest extends jspb.Message {
  getGroupId(): number;
  setGroupId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetGroupRequest): GetGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGroupRequest;
  static deserializeBinaryFromReader(message: GetGroupRequest, reader: jspb.BinaryReader): GetGroupRequest;
}

export namespace GetGroupRequest {
  export type AsObject = {
    groupId: number,
  }
}

export class GetGroupResponse extends jspb.Message {
  hasGroup(): boolean;
  clearGroup(): void;
  getGroup(): v1_message_common_dtos_pb.Group | undefined;
  setGroup(value?: v1_message_common_dtos_pb.Group): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetGroupResponse): GetGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGroupResponse;
  static deserializeBinaryFromReader(message: GetGroupResponse, reader: jspb.BinaryReader): GetGroupResponse;
}

export namespace GetGroupResponse {
  export type AsObject = {
    group?: v1_message_common_dtos_pb.Group.AsObject,
  }
}

export class CreateGroupRequest extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearUserIdsList(): void;
  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): void;
  addUserIds(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateGroupRequest): CreateGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateGroupRequest;
  static deserializeBinaryFromReader(message: CreateGroupRequest, reader: jspb.BinaryReader): CreateGroupRequest;
}

export namespace CreateGroupRequest {
  export type AsObject = {
    name: string,
    description: string,
    userIdsList: Array<string>,
  }
}

export class CreateGroupResponse extends jspb.Message {
  hasGroup(): boolean;
  clearGroup(): void;
  getGroup(): v1_message_common_dtos_pb.Group | undefined;
  setGroup(value?: v1_message_common_dtos_pb.Group): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateGroupResponse): CreateGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateGroupResponse;
  static deserializeBinaryFromReader(message: CreateGroupResponse, reader: jspb.BinaryReader): CreateGroupResponse;
}

export namespace CreateGroupResponse {
  export type AsObject = {
    group?: v1_message_common_dtos_pb.Group.AsObject,
  }
}

export class UpdateGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearUserIdsList(): void;
  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): void;
  addUserIds(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateGroupRequest): UpdateGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateGroupRequest;
  static deserializeBinaryFromReader(message: UpdateGroupRequest, reader: jspb.BinaryReader): UpdateGroupRequest;
}

export namespace UpdateGroupRequest {
  export type AsObject = {
    id: number,
    name: string,
    description: string,
    userIdsList: Array<string>,
  }
}

export class UpdateGroupResponse extends jspb.Message {
  hasGroup(): boolean;
  clearGroup(): void;
  getGroup(): v1_message_common_dtos_pb.Group | undefined;
  setGroup(value?: v1_message_common_dtos_pb.Group): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateGroupResponse): UpdateGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateGroupResponse;
  static deserializeBinaryFromReader(message: UpdateGroupResponse, reader: jspb.BinaryReader): UpdateGroupResponse;
}

export namespace UpdateGroupResponse {
  export type AsObject = {
    group?: v1_message_common_dtos_pb.Group.AsObject,
  }
}

export class ListGroupsRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationRequest): void;

  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): GroupFilter | undefined;
  setFilter(value?: GroupFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_message_common_common_pb.Sort | undefined;
  setSort(value?: v1_message_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupsRequest): ListGroupsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupsRequest;
  static deserializeBinaryFromReader(message: ListGroupsRequest, reader: jspb.BinaryReader): ListGroupsRequest;
}

export namespace ListGroupsRequest {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationRequest.AsObject,
    filter?: GroupFilter.AsObject,
    sort?: v1_message_common_common_pb.Sort.AsObject,
  }
}

export class GroupFilter extends jspb.Message {
  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasActive(): boolean;
  clearActive(): void;
  getActive(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setActive(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: GroupFilter): GroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupFilter;
  static deserializeBinaryFromReader(message: GroupFilter, reader: jspb.BinaryReader): GroupFilter;
}

export namespace GroupFilter {
  export type AsObject = {
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    active?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListGroupsResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationResponse): void;

  clearGroupsList(): void;
  getGroupsList(): Array<v1_message_common_dtos_pb.GroupBrief>;
  setGroupsList(value: Array<v1_message_common_dtos_pb.GroupBrief>): void;
  addGroups(value?: v1_message_common_dtos_pb.GroupBrief, index?: number): v1_message_common_dtos_pb.GroupBrief;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupsResponse): ListGroupsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupsResponse;
  static deserializeBinaryFromReader(message: ListGroupsResponse, reader: jspb.BinaryReader): ListGroupsResponse;
}

export namespace ListGroupsResponse {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationResponse.AsObject,
    groupsList: Array<v1_message_common_dtos_pb.GroupBrief.AsObject>,
  }
}

export class DeleteGroupsRequest extends jspb.Message {
  getGroupId(): number;
  setGroupId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteGroupsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteGroupsRequest): DeleteGroupsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteGroupsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteGroupsRequest;
  static deserializeBinaryFromReader(message: DeleteGroupsRequest, reader: jspb.BinaryReader): DeleteGroupsRequest;
}

export namespace DeleteGroupsRequest {
  export type AsObject = {
    groupId: number,
  }
}

export class MutingGroupRequest extends jspb.Message {
  getGroupId(): number;
  setGroupId(value: number): void;

  getUserId(): string;
  setUserId(value: string): void;

  getMute(): boolean;
  setMute(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MutingGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: MutingGroupRequest): MutingGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MutingGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MutingGroupRequest;
  static deserializeBinaryFromReader(message: MutingGroupRequest, reader: jspb.BinaryReader): MutingGroupRequest;
}

export namespace MutingGroupRequest {
  export type AsObject = {
    groupId: number,
    userId: string,
    mute: boolean,
  }
}

export class AddUserGroupRequest extends jspb.Message {
  getGroupId(): number;
  setGroupId(value: number): void;

  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddUserGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddUserGroupRequest): AddUserGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddUserGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddUserGroupRequest;
  static deserializeBinaryFromReader(message: AddUserGroupRequest, reader: jspb.BinaryReader): AddUserGroupRequest;
}

export namespace AddUserGroupRequest {
  export type AsObject = {
    groupId: number,
    userId: string,
  }
}

export class DeleteUserGroupRequest extends jspb.Message {
  getGroupId(): number;
  setGroupId(value: number): void;

  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteUserGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteUserGroupRequest): DeleteUserGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteUserGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteUserGroupRequest;
  static deserializeBinaryFromReader(message: DeleteUserGroupRequest, reader: jspb.BinaryReader): DeleteUserGroupRequest;
}

export namespace DeleteUserGroupRequest {
  export type AsObject = {
    groupId: number,
    userId: string,
  }
}

export class GetUserGroupsRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationRequest): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_message_common_common_pb.Sort | undefined;
  setSort(value?: v1_message_common_common_pb.Sort): void;

  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserGroupsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserGroupsRequest): GetUserGroupsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserGroupsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserGroupsRequest;
  static deserializeBinaryFromReader(message: GetUserGroupsRequest, reader: jspb.BinaryReader): GetUserGroupsRequest;
}

export namespace GetUserGroupsRequest {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationRequest.AsObject,
    sort?: v1_message_common_common_pb.Sort.AsObject,
    userId: string,
  }
}

export class GetUserGroupsResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationResponse): void;

  clearGroupsList(): void;
  getGroupsList(): Array<v1_message_common_dtos_pb.GroupBrief>;
  setGroupsList(value: Array<v1_message_common_dtos_pb.GroupBrief>): void;
  addGroups(value?: v1_message_common_dtos_pb.GroupBrief, index?: number): v1_message_common_dtos_pb.GroupBrief;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserGroupsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserGroupsResponse): GetUserGroupsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserGroupsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserGroupsResponse;
  static deserializeBinaryFromReader(message: GetUserGroupsResponse, reader: jspb.BinaryReader): GetUserGroupsResponse;
}

export namespace GetUserGroupsResponse {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationResponse.AsObject,
    groupsList: Array<v1_message_common_dtos_pb.GroupBrief.AsObject>,
  }
}

export class GetHistoryRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationRequest): void;

  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): GetHistoryFilter | undefined;
  setFilter(value?: GetHistoryFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_message_common_common_pb.Sort | undefined;
  setSort(value?: v1_message_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHistoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetHistoryRequest): GetHistoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHistoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHistoryRequest;
  static deserializeBinaryFromReader(message: GetHistoryRequest, reader: jspb.BinaryReader): GetHistoryRequest;
}

export namespace GetHistoryRequest {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationRequest.AsObject,
    filter?: GetHistoryFilter.AsObject,
    sort?: v1_message_common_common_pb.Sort.AsObject,
  }
}

export class GetHistoryFilter extends jspb.Message {
  hasGroupId(): boolean;
  clearGroupId(): void;
  getGroupId(): google_protobuf_wrappers_pb.UInt64Value | undefined;
  setGroupId(value?: google_protobuf_wrappers_pb.UInt64Value): void;

  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFrom(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_wrappers_pb.StringValue | undefined;
  setTo(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasRange(): boolean;
  clearRange(): void;
  getRange(): v1_message_common_common_pb.TimeRange | undefined;
  setRange(value?: v1_message_common_common_pb.TimeRange): void;

  hasByUser(): boolean;
  clearByUser(): void;
  getByUser(): google_protobuf_wrappers_pb.StringValue | undefined;
  setByUser(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHistoryFilter.AsObject;
  static toObject(includeInstance: boolean, msg: GetHistoryFilter): GetHistoryFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHistoryFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHistoryFilter;
  static deserializeBinaryFromReader(message: GetHistoryFilter, reader: jspb.BinaryReader): GetHistoryFilter;
}

export namespace GetHistoryFilter {
  export type AsObject = {
    groupId?: google_protobuf_wrappers_pb.UInt64Value.AsObject,
    from?: google_protobuf_wrappers_pb.StringValue.AsObject,
    to?: google_protobuf_wrappers_pb.StringValue.AsObject,
    range?: v1_message_common_common_pb.TimeRange.AsObject,
    byUser?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetHistoryResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_message_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_message_common_common_pb.PaginationResponse): void;

  clearMessageList(): void;
  getMessageList(): Array<v1_message_common_dtos_pb.ChatMessage>;
  setMessageList(value: Array<v1_message_common_dtos_pb.ChatMessage>): void;
  addMessage(value?: v1_message_common_dtos_pb.ChatMessage, index?: number): v1_message_common_dtos_pb.ChatMessage;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHistoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetHistoryResponse): GetHistoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHistoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHistoryResponse;
  static deserializeBinaryFromReader(message: GetHistoryResponse, reader: jspb.BinaryReader): GetHistoryResponse;
}

export namespace GetHistoryResponse {
  export type AsObject = {
    pagination?: v1_message_common_common_pb.PaginationResponse.AsObject,
    messageList: Array<v1_message_common_dtos_pb.ChatMessage.AsObject>,
  }
}

export class SendMessageRequest extends jspb.Message {
  getFrom(): string;
  setFrom(value: string): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_wrappers_pb.StringValue | undefined;
  setTo(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasGroup(): boolean;
  clearGroup(): void;
  getGroup(): google_protobuf_wrappers_pb.UInt64Value | undefined;
  setGroup(value?: google_protobuf_wrappers_pb.UInt64Value): void;

  getMessage(): string;
  setMessage(value: string): void;

  hasSendAt(): boolean;
  clearSendAt(): void;
  getSendAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setSendAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendMessageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendMessageRequest): SendMessageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SendMessageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendMessageRequest;
  static deserializeBinaryFromReader(message: SendMessageRequest, reader: jspb.BinaryReader): SendMessageRequest;
}

export namespace SendMessageRequest {
  export type AsObject = {
    from: string,
    to?: google_protobuf_wrappers_pb.StringValue.AsObject,
    group?: google_protobuf_wrappers_pb.UInt64Value.AsObject,
    message: string,
    sendAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class SendMessageResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  hasMessage(): boolean;
  clearMessage(): void;
  getMessage(): v1_message_common_dtos_pb.ChatMessage | undefined;
  setMessage(value?: v1_message_common_dtos_pb.ChatMessage): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendMessageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SendMessageResponse): SendMessageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SendMessageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendMessageResponse;
  static deserializeBinaryFromReader(message: SendMessageResponse, reader: jspb.BinaryReader): SendMessageResponse;
}

export namespace SendMessageResponse {
  export type AsObject = {
    id: number,
    message?: v1_message_common_dtos_pb.ChatMessage.AsObject,
  }
}

export class UpdateMessageRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateMessageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateMessageRequest): UpdateMessageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateMessageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateMessageRequest;
  static deserializeBinaryFromReader(message: UpdateMessageRequest, reader: jspb.BinaryReader): UpdateMessageRequest;
}

export namespace UpdateMessageRequest {
  export type AsObject = {
    id: number,
    message: string,
  }
}

export class DeleteMessageRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteMessageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteMessageRequest): DeleteMessageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteMessageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteMessageRequest;
  static deserializeBinaryFromReader(message: DeleteMessageRequest, reader: jspb.BinaryReader): DeleteMessageRequest;
}

export namespace DeleteMessageRequest {
  export type AsObject = {
    id: number,
  }
}

