// package: fcp.message.v1
// file: v1/message_common/dtos.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class Group extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getAvatar(): string;
  setAvatar(value: string): void;

  clearUsersList(): void;
  getUsersList(): Array<UserInGroup>;
  setUsersList(value: Array<UserInGroup>): void;
  addUsers(value?: UserInGroup, index?: number): UserInGroup;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  hasCompanion(): boolean;
  clearCompanion(): void;
  getCompanion(): UserBriefForMessage | undefined;
  setCompanion(value?: UserBriefForMessage): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Group.AsObject;
  static toObject(includeInstance: boolean, msg: Group): Group.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Group, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Group;
  static deserializeBinaryFromReader(message: Group, reader: jspb.BinaryReader): Group;
}

export namespace Group {
  export type AsObject = {
    id: number,
    name: string,
    description: string,
    avatar: string,
    usersList: Array<UserInGroup.AsObject>,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
    companion?: UserBriefForMessage.AsObject,
  }
}

export class UserBriefForMessage extends jspb.Message {
  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserBriefForMessage.AsObject;
  static toObject(includeInstance: boolean, msg: UserBriefForMessage): UserBriefForMessage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserBriefForMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserBriefForMessage;
  static deserializeBinaryFromReader(message: UserBriefForMessage, reader: jspb.BinaryReader): UserBriefForMessage;
}

export namespace UserBriefForMessage {
  export type AsObject = {
    phoneNumber: string,
    firstName: string,
    lastName: string,
    email: string,
  }
}

export class GroupBrief extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getAvatar(): string;
  setAvatar(value: string): void;

  clearUsersList(): void;
  getUsersList(): Array<UserInGroup>;
  setUsersList(value: Array<UserInGroup>): void;
  addUsers(value?: UserInGroup, index?: number): UserInGroup;

  getState(): number;
  setState(value: number): void;

  hasCompanion(): boolean;
  clearCompanion(): void;
  getCompanion(): UserBriefForMessage | undefined;
  setCompanion(value?: UserBriefForMessage): void;

  hasLastMessageAt(): boolean;
  clearLastMessageAt(): void;
  getLastMessageAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setLastMessageAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupBrief.AsObject;
  static toObject(includeInstance: boolean, msg: GroupBrief): GroupBrief.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupBrief, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupBrief;
  static deserializeBinaryFromReader(message: GroupBrief, reader: jspb.BinaryReader): GroupBrief;
}

export namespace GroupBrief {
  export type AsObject = {
    id: number,
    name: string,
    description: string,
    avatar: string,
    usersList: Array<UserInGroup.AsObject>,
    state: number,
    companion?: UserBriefForMessage.AsObject,
    lastMessageAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class UserInGroup extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getMute(): boolean;
  setMute(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserInGroup.AsObject;
  static toObject(includeInstance: boolean, msg: UserInGroup): UserInGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserInGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserInGroup;
  static deserializeBinaryFromReader(message: UserInGroup, reader: jspb.BinaryReader): UserInGroup;
}

export namespace UserInGroup {
  export type AsObject = {
    userId: string,
    mute: boolean,
  }
}

export class ChatMessage extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGroupId(): number;
  setGroupId(value: number): void;

  getFrom(): string;
  setFrom(value: string): void;

  getTo(): string;
  setTo(value: string): void;

  getMessage(): string;
  setMessage(value: string): void;

  hasSendAt(): boolean;
  clearSendAt(): void;
  getSendAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setSendAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasEditAt(): boolean;
  clearEditAt(): void;
  getEditAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEditAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChatMessage.AsObject;
  static toObject(includeInstance: boolean, msg: ChatMessage): ChatMessage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ChatMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChatMessage;
  static deserializeBinaryFromReader(message: ChatMessage, reader: jspb.BinaryReader): ChatMessage;
}

export namespace ChatMessage {
  export type AsObject = {
    id: number,
    groupId: number,
    from: string,
    to: string,
    message: string,
    sendAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    editAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GroupsChange extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  clearUserIdsList(): void;
  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): void;
  addUserIds(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupsChange.AsObject;
  static toObject(includeInstance: boolean, msg: GroupsChange): GroupsChange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupsChange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupsChange;
  static deserializeBinaryFromReader(message: GroupsChange, reader: jspb.BinaryReader): GroupsChange;
}

export namespace GroupsChange {
  export type AsObject = {
    id: number,
    userIdsList: Array<string>,
  }
}

export class UserStatus extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getBlock(): boolean;
  setBlock(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserStatus.AsObject;
  static toObject(includeInstance: boolean, msg: UserStatus): UserStatus.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserStatus;
  static deserializeBinaryFromReader(message: UserStatus, reader: jspb.BinaryReader): UserStatus;
}

export namespace UserStatus {
  export type AsObject = {
    id: string,
    block: boolean,
  }
}

export interface GroupStateMap {
  GROUPSTATE_ACTIVE: 0;
  GROUPSTATE_INACTIVE: 1;
}

export const GroupState: GroupStateMap;

